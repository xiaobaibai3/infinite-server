<?php
/**
 * @File:   index.php
 * @Author: Haozhe Xie
 * @Date:   2024-02-01 18:08:07
 * @Last Modified by: Haozhe Xie
 * @Last Modified at: 2024-02-06 08:20:08
 * @Email:  root@haozhexie.com
 */

error_reporting(0);
require_once('helpers.php');

header('Access-Control-Allow-Origin: *');

$request_uri   = $_SERVER["REQUEST_URI"] ?? "";
$request_url   = parse_url($request_uri);
$request_paths = explode("/", $request_url["path"]);
$request_path  = "/" . end($request_paths);

$config  = array(
    "interval" => 3,
    "keys"     => array(
        "distname", "cpuinfo",
        "time", "uptime", "meminfo", "loadavg", "diskinfo", "netdev"
    )
);
$servers = json_decode(@file_get_contents("servers.json"), true);

// Simple Router
switch ($request_path) {
    case "/":
        echo @file_get_contents("default.html");
        break;
    case "/servers":
        echo json_encode(get_server_info($servers, $config));
        break;
    case "/status":
        sse_loop($servers, $config);
        break;
    default:
        http_response_code(404);
        break;
}
