<?php
/**
 * @File:   status.php
 * @Author: Haozhe Xie
 * @Date:   2024-02-01 16:18:32
 * @Last Modified by: Haozhe Xie
 * @Last Modified at: 2024-02-06 08:18:16
 * @Email:  root@haozhexie.com
 */

error_reporting(0);

header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
header("Cache-Control: no-cache, must-revalidate");

$request_uri = $_SERVER["REQUEST_URI"] ?? "";
$request_url = parse_url($request_uri);

parse_str($request_url["query"] ?? "", $params);
$params = array_change_key_case($params, CASE_LOWER);
$key = $params["k"] ?? $argv[1] ?? "";
if (empty($key)) {
    http_response_code(404);
    exit();
}

function human_filesize($bytes) {
    if ($bytes == 0) {
        return '0 B';
    }

    $units = array('B','K','M','G','T');
    $size = '';

    while ($bytes > 0 && count($units) > 0) {
        $size = strval($bytes % 1024) . ' ' .array_shift($units) . ' ' . $size;
        $bytes = intval($bytes / 1024);
    }
    return $size;
}

function get_remote_addr() {
    if (isset($_SERVER["HTTP_X_REAL_IP"])) {
        return $_SERVER["HTTP_X_REAL_IP"];
    } else if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
        return preg_replace('/^.+,\s*/', '', $_SERVER["HTTP_X_FORWARDED_FOR"]);
    } else {
        return $_SERVER["REMOTE_ADDR"];
    }
}

function get_server_addr() {
    if ($_SERVER["SERVER_ADDR"] != "127.0.0.1") {
        return $_SERVER["SERVER_ADDR"];
    } else {
        return gethostbyname(php_uname('n'));
    }
}

function get_stat() {
    $content = file('/proc/stat');
    $array = array_shift($content);
    $array = preg_split('/\s+/', trim($array));
    return array_slice($array, 1);
}

function get_sockstat() {
    $info = array();
    $content = file('/proc/net/sockstat');
    foreach ($content as $line) {
        $parts = explode(':', $line);
        $key = trim($parts[0]);
        $values = preg_split('/\s+/', trim($parts[1]));
        $info[$key] = array();
        for ($i = 0; $i < count($values); $i += 2) {
            $info[$key][$values[$i]] = $values[$i+1];
        }
    }
    return $info;
}

function get_cpuinfo() {
    $info = array();

    if (!($str = @file("/proc/cpuinfo"))) {
        return false;
    }
    $str = implode("", $str);
    @preg_match_all("/processor\s{0,}\:+\s{0,}([\w\s\)\(\@.-]+)([\r\n]+)/s", $str, $processor);
    @preg_match_all("/model\s+name\s{0,}\:+\s{0,}([\w\s\)\(\@.-]+)([\r\n]+)/s", $str, $model);

    if (count($model[0]) == 0) {
        @preg_match_all("/Hardware\s{0,}\:+\s{0,}([\w\s\)\(\@.-]+)([\r\n]+)/s", $str, $model);
    }
    @preg_match_all("/cpu\s+MHz\s{0,}\:+\s{0,}([\d\.]+)[\r\n]+/", $str, $mhz);

    if (count($mhz[0]) == 0) {
        $values = @file("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq");
        $mhz = array("", array(sprintf('%.3f', intval($values[0])/1000)));
    }

    @preg_match_all("/cache\s+size\s{0,}\:+\s{0,}([\d\.]+\s{0,}[A-Z]+[\r\n]+)/", $str, $cache);
    @preg_match_all("/(?i)bogomips\s{0,}\:+\s{0,}([\d\.]+)[\r\n]+/", $str, $bogomips);
    @preg_match_all("/(?i)(flags|Features)\s{0,}\:+\s{0,}(.+)[\r\n]+/", $str, $flags);

    if (is_array($model[1])) {
        $info['num'] = sizeof($processor[1]);
        $info['model'] = count($model[1]) ? $model[1][0] : "Unknown";
        $info['frequency'] = $mhz[1][0];
        # $info['bogomips'] = $bogomips[1][0];
        # if (count($cache[0]) > 0) {
        # 	$info['l2cache'] = trim($cache[1][0]);
        # }
        # $info['flags'] = $flags[2][0];
    }
    return $info;
}

function get_uptime() {
    if (!($str = @file('/proc/uptime'))) {
        return false;
    }
    $uptime = '';
    $str = explode(' ', implode('', $str));
    $str = trim($str[0]);
    $min = $str / 60;
    $hours = $min / 60;
    $days = floor($hours / 24);
    $hours = floor($hours - ($days * 24));
    $min = floor($min - ($days * 60 * 24) - ($hours * 60));
    $zh = (substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) === 'zh');
    $duint = !$zh ? (' day'. ($days > 1 ? 's ':' ')) : '天';
    $huint = !$zh ? (' hour'. ($hours > 1 ? 's ':' ')) : '小时';
    $muint = !$zh ? (' minute'. ($min > 1 ? 's ':' ')) : '分钟';

    if ($days !== 0) {
        $uptime = $days.$duint;
    }
    if ($hours !== 0) {
        $uptime .= $hours.$huint;
    }
    $uptime .= $min.$muint;
    return $uptime;
}

function get_tempinfo() {
    $info = array();

    if ($str = @file('/sys/class/thermal/thermal_zone0/temp')) {
        $info['cpu'] = $str[0]/1000.0;
    }
    return $info;
}

function get_meminfo() {
    $info = array();

    if (!($str = @file('/proc/meminfo'))) {
        return false;
    }
    $str = implode('', $str);
    preg_match_all("/MemTotal\s{0,}\:+\s{0,}([\d\.]+).+?MemFree\s{0,}\:+\s{0,}([\d\.]+).+?Cached\s{0,}\:+\s{0,}([\d\.]+).+?SwapTotal\s{0,}\:+\s{0,}([\d\.]+).+?SwapFree\s{0,}\:+\s{0,}([\d\.]+)/s", $str, $buf);
    preg_match_all("/Buffers\s{0,}\:+\s{0,}([\d\.]+)/s", $str, $buffers);

    $info['memTotal'] = round($buf[1][0]/1024, 0);
    $info['memFree'] = round($buf[2][0]/1024, 0);
    $info['memBuffers'] = round($buffers[1][0]/1024, 0);
    $info['memCached'] = round($buf[3][0]/1024, 0);
    $info['memUsed'] = round($info['memTotal']-$info['memFree']-$info['memBuffers']-$info['memCached'], 0);
    $info['memUsedPercent'] = (floatval($info['memTotal'])!=0)?round($info['memUsed']/$info['memTotal']*100, 0):0;
    $info['memBuffersPercent'] = (floatval($info['memTotal'])!=0)?round($info['memBuffers']/$info['memTotal']*100, 0):0;
    $info['memCachedPercent'] = (floatval($info['memTotal'])!=0)?round($info['memCached']/$info['memTotal']*100, 0):0;

    $info['swapTotal'] = round($buf[4][0]/1024, 0);
    $info['swapFree'] = round($buf[5][0]/1024, 0);
    $info['swapUsed'] = round($info['swapTotal']-$info['swapFree'], 0);
    $info['swapPercent'] = (floatval($info['swapTotal'])!=0)?round($info['swapUsed']/$info['swapTotal']*100, 0):0;

    foreach ($info as $key => $value) {
        if (str_contains($key, 'Percent')) {
            continue;
        }
        if ($value < 1024) {
            $info[$key] .= ' M';
        } else {
            $info[$key] = round($value/1024, 1) . ' G';
        }
    }
    return $info;
}

function get_loadavg() {
    if (!($str = @file('/proc/loadavg'))) {
        return false;
    }
    $str = explode(' ', implode('', $str));
    $str = array_chunk($str, 3);
    $loadavg = implode(' ', $str[0]);
    return explode(" ", $loadavg);
}

function get_distname() {
    foreach (array('redhat', 'centos', 'system') as $name) {
        if ($content = @file("/etc/$name-release")) {
            return array_shift($content);
        }
    }
    $release_info = @parse_ini_file('/etc/os-release');
    if (isset($release_info['DISTRIB_DESCRIPTION'])) {
        return $release_info['DISTRIB_DESCRIPTION'];
    }
    if (isset($release_info['PRETTY_NAME'])) {
        return $release_info['PRETTY_NAME'];
    }
    if ($content = file('/etc/issue')) {
        foreach ($content as $i => $line) {
            if (strpos($line, 'elcome to ') > 0) {
                return substr($line, 11);
            }
        }
    }

    return php_uname('s').' '.php_uname('r');
}

function get_boardinfo() {
    $info = array();

    if ($content = @file('/sys/class/dmi/id/bios_vendor', FILE_IGNORE_NEW_LINES)) {
        $info['BIOSVendor'] = array_shift($content);
    }
    if ($content = @file('/sys/class/dmi/id/bios_version', FILE_IGNORE_NEW_LINES)) {
        $info['BIOSVersion'] = array_shift($content);
    }
    if ($content = @file('/sys/class/dmi/id/bios_date', FILE_IGNORE_NEW_LINES)) {
        $info['BIOSDate'] = array_shift($content);
    }

    if ($content = @file('/sys/class/dmi/id/board_vendor', FILE_IGNORE_NEW_LINES)) {
        $info['boardVendor'] = array_shift($content);
    }
    if (!isset($info['boardVendor'])) {
        if ($content = @file('/sys/class/dmi/id/product_name', FILE_IGNORE_NEW_LINES)) {
            $info['boardVendor'] = array_shift($content);
        }
    }
    if ($content = @file('/sys/class/dmi/id/board_name', FILE_IGNORE_NEW_LINES)) {
        $info['boardName'] = array_shift($content);
    }
    if ($content = @file('/sys/class/dmi/id/board_version', FILE_IGNORE_NEW_LINES)) {
        $info['boardVersion'] = array_shift($content);
    }
    return $info;
}

function get_mount_points() {
    $disks = array();
    $mount_points = array();
    $lines = explode("\n", `mount`);
    foreach ($lines as $line) {
        if (str_contains($line, '/dev') && str_contains($line, 'ext4')) {
            $tokens       = explode(" ", $line);
            $disk        = $tokens[0];
            $mount_point = $tokens[2];
            if (!in_array($disk, $disks)) {
                array_push($disks, $disk);
                array_push($mount_points, $mount_point);
            }
        }
    }
    return $mount_points;
}

function get_diskinfo() {
    $info = array(
        'diskTotal' => 0,
        'diskFree'  => 0,
    );
    $mount_points = get_mount_points();
    foreach ($mount_points as $mp) {
        $info['diskTotal'] += round(@disk_total_space($mp)/(1024*1024*1024), 0);
        $info['diskFree'] += round(@disk_free_space($mp)/(1024*1024*1024), 0);
    }
    $info['diskUsed'] = round($info['diskTotal'] - $info['diskFree'], 0);
    $info['diskPercent'] = 0;
    if (floatval($info['diskTotal']) != 0) {
        $info['diskPercent'] = round($info['diskUsed']/$info['diskTotal']*100, 0);
    }
    foreach ($info as $key => $value) {
        if (str_contains($key, 'Percent')) {
            continue;
        }
        if ($value < 1024) {
            $info[$key] .= ' G';
        } else {
            $info[$key] = round($value/1024, 1) . ' T';
        }
    }
    return $info;
}

function get_netdev() {
    $info = array(
        'ts' => round(microtime(true) * 1000),
        'rx' => 0,
        'tx' => 0,
    );

    $strs = @file('/proc/net/dev');
    for ($i = 2; $i < count($strs); $i++ ) {
        $parts = preg_split('/\s+/', trim($strs[$i]));
        $dev = trim($parts[0], ':');
        if (!str_starts_with($dev, "eth") && !str_starts_with($dev, "ens")) {
            continue;
        }
        $info['rx'] += $parts[1];
        $info['tx'] += $parts[9];
        # $info[$dev] = array(
        # 	'rx' => $parts[1],
        # 	'tx' => $parts[9],
        # 	'human_rx' => human_filesize($parts[1]),
        # 	'human_tx' => human_filesize($parts[9]),
        # );
    }
    return $info;
}

function get_netarp() {
    $info = array();
    $seen = array();
    $strs = @file('/proc/net/arp');
    for ($i = 1; $i < count($strs); $i++ ) {
        $parts = preg_split('/\s+/', $strs[$i]);
        if ('0x2' == $parts[2] && !isset($seen[$parts[3]])) {
            $seen[$parts[3]] = true;
            $info[$parts[0]] = array(
                'hw_type' => $parts[1]=='0x1'?'ether':$parts[1],
                'hw_addr' => $parts[3],
                'device' => $parts[5],
            );
        }
    }
    return $info;
}

switch ($key) {
    case 's':
        echo json_encode(array(
            'uname' => php_uname(),
            'distname' => trim(get_distname()),
            'cpuinfo' => get_cpuinfo(),
            # 'netarp' => get_netarp(),
        ));
        exit;
    case 'r':
        echo json_encode(array(
            'time' => time(),
            'uptime' => get_uptime(),
            'meminfo' => get_meminfo(),
            'loadavg' => get_loadavg(),
            'diskinfo' => get_diskinfo(),
            'netdev' => get_netdev(),
        ));
        exit;
}
