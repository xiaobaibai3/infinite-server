<?php
/**
 * @File:   helpers.php
 * @Author: Haozhe Xie
 * @Date:   2024-02-01 18:18:36
 * @Last Modified by: Haozhe Xie
 * @Last Modified at: 2024-02-06 08:17:40
 * @Email:  root@haozhexie.com
 */

function get_server_info($servers, $cfg) {
    $info = array();
    foreach ($servers as $name => $values) {
        $server = json_decode(@file_get_contents(sprintf("%s?k=s", $values["url"])), true);
        foreach ($server as $k => $v) {
            if (in_array($k, $cfg["keys"])) {
                $info[$name][$k] = $v;
            }
        }
        foreach ($values as $k => $v) {
            if ($k == "url") {
                continue;
            }
            $info[$name][$k] = $v;
        }
    }
    return $info;
}

function get_server_status($servers, $cfg) {
    $status = array();
    foreach ($servers as $name => $values) {
        $statusp[$name] = array();
        $_status = json_decode(@file_get_contents(sprintf("%s?k=r", $values["url"])), true);
        foreach ($_status as $k => $v) {
            if (in_array($k, $cfg["keys"])) {
                $status[$name][$k] = $v;
            }
        }
    }
    return $status;
}

function sse_loop($servers, $cfg) {
    header("Cache-Control: no-store");
    header("Content-Type: text/event-stream");
    header('X-Accel-Buffering: no');
    
    // The event loop
    while (true) {
        echo 'data: ' . json_encode(get_server_status($servers, $cfg)) . "\n\n";
 
        ob_end_flush();
        flush(); 
        // Break the loop if the client aborted the connection (closed the page)
        if (connection_aborted()) {
            break;
        }
        sleep($cfg["interval"]);
    }
}
