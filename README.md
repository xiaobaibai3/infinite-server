# Infinite Server

## Introduction

**Infinite Server** is an open-source, super lightweight, and user-friendly server monitoring and operations tool.

Compared to existing server monitoring tools like [Nezha](https://nezha.wiki/) and [MyNodeQuery](https://hub.docker.com/r/jaydenlee2019/mynodequery), Infinite Server stands out with the following highlights.

- No root privileges required.
- Super lightweight: no database or daemon process needed.
- Easy to use, easy to go: Delivered with simple scripts, no need for a complex installer or Docker.

![Preview](https://i.imgur.com/0LahyEO.png)

## Prerequisites

Make sure that you have the web server (Apache or Nginx) and PHP installed.
If not, please refer to the [tutorial](https://www.linode.com/docs/guides/install-php-8-for-apache-and-nginx-on-ubuntu/).

## Installation

Assume that the HTML root directory is `/var/www/html`.

### Agent

For all agents, the PHP script providing server information should be accessible to the public on the Internet.

```
cd /var/www/html
wget https://gitlab.com/hzxie/infinite-server/-/raw/master/status.php
```

The script should be accessiable via `http://YOUR_SERVER_IP/status.php`.

### Dashboard

To set up the dashboard that summarizes all server information, please follow these steps.

#### Clone the Git Repo

```
cd /var/www/html
git clone https://gitlab.com/hzxie/infinite-server
```

#### Edit the Servers Node Config

Edit the server node config file `servers.json`.

```
{
	"Awesome Server Name": {
		"region": "HK",
		"url": "http://localhost/status.php"
	}
}
```

#### Web Server Configuration

If you are using **Apache** web server, add the following content to `.htaccess`.

```apache
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^ index.php [L]
```

If you are using **Nginx** web server, add the following content to your Nginx config.

```nginx
location / {
    proxy_http_version          1.1;
    proxy_buffering             off;
    try_files                   $uri /index.php$is_args$args;
}
```

The web page should be accessiable via `http://YOUR_SERVER_IP/infinite-server`.

## License

The project is open sourced under the MIT license.

